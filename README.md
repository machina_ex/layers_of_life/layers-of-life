# Layers of Life

## Preflight Protokoll Vorstellungstag:

### Wohnwagen Licht
- Wohnwagen LAN anschließen (wichtig, dass das vor dem Strom passiert)
- Wohnwagen Strom anschließen (die Stecker im Lager/Büro)
- Wohnwagen Strom anschließen (Säule)
- jetzt sollte unter mqtt://164.92.244.191/dev/24:0A:C4:26:D6:A8 ein heartbeat sichtbar werden

### Koffer
- Koffer von der Ladestation trennen
- volle lithium Akkus in Koffer einlegen
- in Powerbank einstecken
- ssh 164.92.244.191 > ssh 10.10.10.X > `cd koffer && ./start_it_all.sh`
- der Drucker sollte einen Testausdruck ausdrucken.
- auf dem Dashboard checken ob der Koffer verbunden ist.
- mit `screen -r arduino` checken ob arduino verbunden

### Player
- Player vom Strom trennen
- beliebigen Knopf am Player drücken damit er angeht
- im Dashboard gucken ob der Player auftaucht und dem richtigen Koffer zugeordnet ist sonst mit NICHT retainter message an set/playerXX/koffer kofferX zuordnung ändern.

### Player und Koffer
- den Knopf auf dem Koffer drücken. 
- an allen Playern müsste INTRO zu hören sein. 
- im Dashboard mit Klick auf RESET-State in RESET zurücksetzen 

## Preflight Protokoll vor jeder Vorstellung:
- Papier im Drucker prüfen
  - ist genug da?
  - ist es richtig eingelegt
- Koffer mit `sudo reboot` und `cd koffer && ./start_it_all.sh` neustarten
- Ist der Testausdruck da? 
- sind unter `screen -r arduino` keine Fehler zu sehen?
- Sind die Player online und haben genug Akku (im Zweifel unter `mqtt://164.92.244.191/dev/playerXX/battery` `"tte"` nachschauen, das ist die noch zu erwartende Akkulebensdauer in Minuten die auf +-30 Minuten genau ist, meistens steht da sogar weniger als wirklich noch geht)
- Wenn die Player zwischengeladen werden:
  1. Player mit `sudo shutdown -h now` und anschließendem Tastendruck komplett neustarten. 
  2. Mit `alsamixer` oder `i2cdetect -y 1` prüfen ob der Audiocodec da ist. 
- Im Dashbaord mit Klick auf RESET reset auslösen, damit alle Player auf dem gleichen stand sind.
- Testausdruck abreißen
- mit `screen -r arduino` beobachten wie erster Tastendruck erfolgt. 

## Postflight
1. alle Koffer bis auf einen herunterfahren.
2. mit dem Dashboard testen ob alle player online sind 
3. mit `for n in {01..31}; do ssh pi@player$n -f 'sudo shutdown -h now'; done` von der Koffern aus herunterfahren.
4. an die Ladegeräte stecken
5. wieder bei 2. anfangen falls irgendwelche player wieder online sind...
6. Koffer herunterfahen
7. Koffer abstecken und powerbanks anstecken
8. Akkus aus dem Koffer ins Ladegerät
